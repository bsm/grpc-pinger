default: proto

proto: proto.go proto.rb

proto.go: pinger.proto
	protoc --gogo_out=plugins=grpc:. --proto_path=. $<

proto.rb: pinger.proto
	protoc --ruby_out=. --grpc_out=. --plugin=protoc-gen-grpc=`which grpc_tools_ruby_protoc_plugin` --proto_path=. $<

.PHONY: proto proto.go proto.rb

server.go: keys/server.crt
	@go run go/server.go

server.rb: keys/server.crt
	@./server.rb

client.go: keys/server.crt
	@go run go/client.go

client.rb: keys/server.crt
	@./client.rb

.PHONY: server.go server.rb client.go client.rb

keys/server.crt:
	@mkdir -p $(dir $@)
	openssl req -new -newkey rsa:4096 -days 7200 -nodes -x509 -subj '/C=GB/ST=London/L=London/O=BSM/CN=localhost' -keyout keys/server.key -out $@
