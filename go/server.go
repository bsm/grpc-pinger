package main

import (
	"crypto/tls"
	"log"
	"net"

	"bitbucket.org/bsm/grpc-pinger"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	cert, err := tls.LoadX509KeyPair("keys/server.crt", "keys/server.key")
	if err != nil {
		return err
	}

	l, err := net.Listen("tcp", ":11000")
	if err != nil {
		return err
	}
	defer l.Close()

	log.Printf("listening on %s", l.Addr().String())

	s := grpc.NewServer(grpc.Creds(credentials.NewTLS(&tls.Config{
		Certificates:       []tls.Certificate{cert},
		InsecureSkipVerify: true,
	})))
	pinger.RegisterV1Server(s, server{})
	return s.Serve(l)
}

type server struct{}

func (server) Ping(ctx context.Context, req *pinger.PingRequest) (*pinger.PingResponse, error) {
	msg := req.GetMsg()
	if msg == "" {
		msg = "PONG"
	}
	return &pinger.PingResponse{
		Msg: msg,
	}, nil
}
