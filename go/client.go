package main

import (
	"crypto/tls"
	"fmt"
	"os"

	"bitbucket.org/bsm/grpc-pinger"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func main() {
	if err := run(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func run() error {
	cc, err := grpc.Dial("localhost:11000", grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{
		InsecureSkipVerify: true,
	})))
	if err != nil {
		return err
	}
	defer cc.Close()

	c := pinger.NewV1Client(cc)
	res, err := c.Ping(context.Background(), &pinger.PingRequest{})
	if err != nil {
		return err
	}

	fmt.Println(res.Msg)
	return nil
}
