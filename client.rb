#!/usr/bin/env ruby

$LOAD_PATH << File.expand_path("../", __FILE__)

require 'pinger_services_pb'

creds  = GRPC::Core::ChannelCredentials.new(
  File.read(File.expand_path('../keys/server.crt', __FILE__))
)
client = Pinger::V1::Stub.new 'localhost:11000', creds

req = Pinger::PingRequest.new()
res = client.ping(req)
puts res.msg
