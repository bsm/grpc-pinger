#!/usr/bin/env ruby

$LOAD_PATH << File.expand_path("../", __FILE__)

require 'grpc'
require 'pinger_services_pb'
require 'openssl'

class Server < Pinger::V1::Service
  def ping(req, _)
    msg = req.msg.to_s
    msg = "PONG" if msg == ""

    Pinger::PingResponse.new msg: msg
  end
end

creds  = GRPC::Core::ServerCredentials.new nil, [{
  private_key: File.read(File.expand_path('../keys/server.key', __FILE__)),
  cert_chain:  File.read(File.expand_path('../keys/server.crt', __FILE__)),
}], false

puts "starting on :11000"

server = GRPC::RpcServer.new
server.add_http2_port '0.0.0.0:11000', creds
server.handle(Server.new)
server.run_till_terminated
